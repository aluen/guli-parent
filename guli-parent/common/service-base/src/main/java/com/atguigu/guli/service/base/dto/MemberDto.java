package com.atguigu.guli.service.base.dto;

import lombok.Data;

/**
 * @author captainlhy
 * @create 2021-07-17 13:40
 */
@Data
public class MemberDto {
    private String id;//会员id
    private String mobile;//手机号
    private String nickname;//昵称
}
