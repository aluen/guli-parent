package com.atguigu.guli.service.edu.service;

import com.atguigu.guli.service.edu.entity.Teacher;
import com.atguigu.guli.service.edu.query.TeacherQuery;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author captainlhy
 * @since 2021-07-02
 */
public interface TeacherService extends IService<Teacher> {
    IPage<Teacher> selectPage(Long page, Long limit, TeacherQuery teacherQuery);
    boolean removeAvatarById(String id);

    List<Map<String, Object>> selectNameListByKey(String key);

    /**
     * 根据讲师id获取讲师详情页数据
     * @param id
     * @return
     */
    Map<String, Object> selectTeacherInfoById(String id);
}
