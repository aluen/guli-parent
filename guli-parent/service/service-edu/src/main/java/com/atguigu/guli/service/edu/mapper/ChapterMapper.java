package com.atguigu.guli.service.edu.mapper;

import com.atguigu.guli.service.edu.entity.Chapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author captainlhy
 * @since 2021-07-02
 */
@Repository
public interface ChapterMapper extends BaseMapper<Chapter> {

}
