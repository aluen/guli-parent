package com.atguigu.guli.service.edu.mapper;

import com.atguigu.guli.service.edu.entity.Video;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 课程视频 Mapper 接口
 * </p>
 *
 * @author captainlhy
 * @since 2021-07-02
 */
@Repository
public interface VideoMapper extends BaseMapper<Video> {

}
