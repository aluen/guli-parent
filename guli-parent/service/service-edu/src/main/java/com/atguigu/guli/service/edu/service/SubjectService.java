package com.atguigu.guli.service.edu.service;

import com.atguigu.guli.service.edu.entity.Subject;
import com.atguigu.guli.service.edu.entity.vo.SubjectVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.io.InputStream;
import java.util.List;

/**
 * <p>
 * 课程科目 服务类
 * </p>
 *
 * @author captainlhy
 * @since 2021-07-02
 */
public interface SubjectService extends IService<Subject> {
    /**
     * 读Excel
     * @param inputStream
     */
    void batchImport(InputStream inputStream);

    List<SubjectVo> nestedList();
}
