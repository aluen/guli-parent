package com.atguigu.guli.service.vod.util;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author captainlhy
 * @create 2021-07-12 18:20
 */
@Data
@Component
@ConfigurationProperties(prefix="aliyun.vod")
public class VodProperties {
    private String keyId;
    private String keySecret;
    private String templateGroupId;
    private String workflowId;
}
