package com.atguigu.guli.service.vod.service;

import com.aliyun.oss.ClientException;

import java.io.InputStream;
import java.util.List;

/**
 * @author captainlhy
 * @create 2021-07-12 18:26
 */
public interface VideoService {
    String uploadVideo(InputStream file, String originalFilename);

    void removeVideo(String videoSourceId) throws ClientException, com.aliyuncs.exceptions.ClientException;

    void removeVideoByIdList(List<String> videoIdList) throws com.aliyuncs.exceptions.ClientException;

    String getPlayAuth(String videoSourceId) throws com.aliyuncs.exceptions.ClientException;
}
