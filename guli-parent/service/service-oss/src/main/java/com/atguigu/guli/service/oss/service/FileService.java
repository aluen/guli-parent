package com.atguigu.guli.service.oss.service;

import java.io.InputStream;

/**
 * @author captainlhy
 * @create 2021-07-06 18:03
 */
public interface FileService {

    /**
     * 文件上传至阿里云
     */
    String upload(InputStream inputStream, String module, String oFilename);

    void removeFile(String url);

    String upload(String url, String module);
}
