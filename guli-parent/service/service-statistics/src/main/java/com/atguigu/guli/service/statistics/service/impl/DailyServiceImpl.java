package com.atguigu.guli.service.statistics.service.impl;

import com.atguigu.guli.service.base.result.R;
import com.atguigu.guli.service.statistics.entity.Daily;
import com.atguigu.guli.service.statistics.feign.UcenterMemberService;
import com.atguigu.guli.service.statistics.mapper.DailyMapper;
import com.atguigu.guli.service.statistics.service.DailyService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 网站统计日数据 服务实现类
 * </p>
 *
 * @author captainlhy
 * @since 2021-07-20
 */
@Service
public class DailyServiceImpl extends ServiceImpl<DailyMapper, Daily> implements DailyService {
    @Autowired
    private UcenterMemberService ucenterMemberService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void createStatisticsByDay(String day) {
        //如果当日的统计记录已存在，则删除重新统计|或 提示用户当日记录已存在
        QueryWrapper<Daily> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("date_calculated", day);
        baseMapper.delete(queryWrapper);

        R r = ucenterMemberService.countRegisterNum(day);
        Integer registerNum = (Integer)r.getData().get("registerNum");
        int loginNum = RandomUtils.nextInt(100, 200);
        int videoViewNum = RandomUtils.nextInt(100, 200);
        int courseNum = RandomUtils.nextInt(100, 200);

        //在本地数据库创建统计信息
        Daily daily = new Daily();
        daily.setRegisterNum(registerNum);
        daily.setLoginNum(loginNum);
        daily.setVideoViewNum(videoViewNum);
        daily.setCourseNum(courseNum);
        daily.setDateCalculated(day);

        baseMapper.insert(daily);
    }

    @Override
    public Map<String, Map<String, Object>> getChartData(String begin, String end) {

        Map<String, Map<String, Object>> map = new HashMap<>();

        Map<String, Object> registerNum = getChartDataByType(begin, end, "register_num");
        Map<String, Object> loginNum = getChartDataByType(begin, end, "login_num");
        Map<String, Object> videoViewNum = getChartDataByType(begin, end, "video_view_num");
        Map<String, Object> courseNum = getChartDataByType(begin, end, "course_num");

        map.put("register_num",registerNum);
        map.put("login_num",loginNum);
        map.put("video_view_num",videoViewNum);
        map.put("course_num",courseNum);

        return map;
    }

    private Map<String, Object> getChartDataByType(String begin, String end, String type) {
        Map<String, Object> map = new HashMap<>();

        List<String> xList = new ArrayList<>();
        List<Integer> yList = new ArrayList<>();

        QueryWrapper<Daily> dailyQueryWrapper = new QueryWrapper<>();
        dailyQueryWrapper.select(type, "date_calculated");
        dailyQueryWrapper.between("date_calculated", begin, end);

        List<Map<String, Object>> mapList = baseMapper.selectMaps(dailyQueryWrapper);

        for (Map<String, Object> data : mapList) {
            String dateCalculated = (String)data.get("date_calculated");
            xList.add(dateCalculated);

            Integer count = (Integer) data.get(type);
            yList.add(count);
        }

        map.put("x", xList);
        map.put("y", yList);

        return map;
    }
}
