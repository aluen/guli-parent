package com.atguigu.guli.service.trade.service;

import java.util.Map;

/**
 * @author captainlhy
 * @create 2021-07-19 18:19
 */
public interface WeixinPayService {
    Map<String, Object> createNative(String orderNo, String remoteAddr);
}
