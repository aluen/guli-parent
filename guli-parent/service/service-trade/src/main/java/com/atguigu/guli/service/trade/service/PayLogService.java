package com.atguigu.guli.service.trade.service;

import com.atguigu.guli.service.trade.entity.PayLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 支付日志表 服务类
 * </p>
 *
 * @author captainlhy
 * @since 2021-07-17
 */
public interface PayLogService extends IService<PayLog> {

}
