package com.atguigu.guli.service.trade.feign.fallback;

import com.atguigu.guli.service.base.result.R;
import com.atguigu.guli.service.trade.feign.EduCourseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author captainlhy
 * @create 2021-07-17 16:16
 */
@Slf4j
@Service
public class EduCourseServiceFallBack implements EduCourseService {
    @Override
    public R getCourseDtoById(String courseId) {
        log.error("熔断保护");
        return R.error();
    }

    @Override
    public R updateBuyCountById(String id) {
        log.warn("熔断器被执行");
        return R.error();
    }
}
