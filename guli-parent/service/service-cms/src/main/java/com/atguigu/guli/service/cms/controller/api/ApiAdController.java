package com.atguigu.guli.service.cms.controller.api;

import com.atguigu.guli.service.base.result.R;
import com.atguigu.guli.service.cms.entity.Ad;
import com.atguigu.guli.service.cms.service.AdService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author captainlhy
 * @create 2021-07-13 19:34
 */
//@CrossOrigin //解决跨域问题
@Api(tags = "广告推荐")
@RestController
@RequestMapping("/api/cms/ad")
public class ApiAdController {

    @Autowired
    private AdService adService;

    @Autowired
    private RedisTemplate redisTemplate;
    @ApiOperation("根据推荐位id显示广告推荐")
    @GetMapping("list/{adTypeId}")
    public R listByAdTypeId(@ApiParam(value = "推荐位id", required = true) @PathVariable String adTypeId) {

        List<Ad> ads = adService.selectByAdTypeId(adTypeId);
        return R.ok().data("items", ads);
    }

    @PostMapping("save-test")
    public R saveAd(@RequestBody Ad ad){
        redisTemplate.opsForValue().set("ad", ad);
        return R.ok();
    }

    //信息的分类存储1
    @PostMapping("save-code")
    public R saveCode(){
        redisTemplate.opsForValue().set("code:13766816630", "1234",  5, TimeUnit.MINUTES); //设置过期时间
        redisTemplate.opsForValue().set("code:13766816631", "1235");
        return R.ok();
    }
    //信息的分类存储2
    @PostMapping("save-user")
    public R saveUser(){
        redisTemplate.opsForValue().set("info:13766816630", "helen");
        redisTemplate.opsForValue().set("info:13766816631", "annie");
        return R.ok();
    }

    @GetMapping("get-test/{key}")
    public R getAd(@PathVariable String key){
        Ad ad = (Ad)redisTemplate.opsForValue().get(key);
        return R.ok().data("ad", ad);
    }

    @DeleteMapping("remove-test/{key}")
    public R removeAd(@PathVariable String key){
        Boolean delete = redisTemplate.delete(key);
        System.out.println(delete);//是否删除成功
        Boolean hasKey = redisTemplate.hasKey(key);
        System.out.println(hasKey);//key是否存在
        return R.ok();
    }
}