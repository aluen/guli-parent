package com.atguigu.guli.service.sms.service.impl;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.atguigu.guli.service.base.exception.GuliException;
import com.atguigu.guli.service.base.result.ResultCodeEnum;
import com.atguigu.guli.service.sms.service.SmsService;
import com.atguigu.guli.service.sms.util.SmsProperties;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * @author captainlhy
 * @create 2021-07-14 19:10
 */
@Service
@Slf4j
public class SmsServiceImpl implements SmsService {
    @Autowired
    private SmsProperties smsProperties;

    @Override
    public void sendSms(String mobile, String checkCode) {
        //创建客户端对象
        DefaultProfile profile = DefaultProfile.getProfile(
                smsProperties.getRegionId(),
                smsProperties.getKeyId(),
                smsProperties.getKeySecret());
        IAcsClient client = new DefaultAcsClient(profile);

        //创建请求修对象
        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("PhoneNumbers", mobile);
        request.putQueryParameter("SignName", smsProperties.getSignName());
        request.putQueryParameter("TemplateCode", smsProperties.getTemplateCode());

        Gson gson = new Gson();
        HashMap<String, String> map = new HashMap<>();
        map.put("code", checkCode);
        String json = gson.toJson(map);
        request.putQueryParameter("TemplateParam", json);
        try {
            CommonResponse response = client.getCommonResponse(request);
            String data = response.getData();
            HashMap<String, String> resultMap = gson.fromJson(data, HashMap.class);

            String code = resultMap.get("Code");
            String message = resultMap.get("Message");

            if ("isv.BUSINESS_LIMIT_CONTROL".equals(code)) {
                log.error("短信发送过于频繁：Code = " + code + ", Message = " + message);
                throw new GuliException(ResultCodeEnum.SMS_SEND_ERROR_BUSINESS_LIMIT_CONTROL);
            }

            if (!"OK".equals(code)) {
                log.error("短信发送失败：Code = " + code + ", Message = " + message);
                throw new GuliException(ResultCodeEnum.SMS_SEND_ERROR);
            }
        } catch (ServerException e) {
            log.error("阿里云短信发送失败");
            log.error(ExceptionUtils.getStackTrace(e));
        } catch (ClientException e) {
            log.error("阿里云短信发送失败");
            log.error(ExceptionUtils.getStackTrace(e));
        }

    }
}

