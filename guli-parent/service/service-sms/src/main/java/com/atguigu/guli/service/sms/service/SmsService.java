package com.atguigu.guli.service.sms.service;

/**
 * @author captainlhy
 * @create 2021-07-14 19:09
 */
public interface SmsService {
    void sendSms(String mobile, String checkCode);
}
